# A very basic website for a UI lesson

This repository contains a very basic website promoting a todo application.

This is for a UI lesson demo.

Branches:

- `master` contains the final result
- `step-0` is the starting point for the demo. On top of this the speaker 
  will build the website